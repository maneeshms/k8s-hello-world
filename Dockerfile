# Import python docker image
FROM python:3.6-alpine

# Set the working directory
WORKDIR /app

# Install flask
RUN pip install flask

# Copy the contents of dir to docker image
COPY . /app

# Run the app
CMD ["python3", "app.py"]
